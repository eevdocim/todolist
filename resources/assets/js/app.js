import Vue from 'vue'
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './views/App'
import Categories from './views/Categories'
import Tasks from './views/Tasks'
import Task from './views/Task'

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'categories',
      component: Categories
    },
    {
      path: '/tasks/:id_category',
      name: 'tasks',
      component: Tasks,
      props: true,
    },
    {
      path: '/task/:id',
      name: 'task',
      component: Task,
      props: true,
    },
  ],
});

const app = new Vue({
  el: '#app',
  components: { App },
  router
});