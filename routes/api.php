<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('tasks', 'API\TaskControlle@index');
Route::get('tasks/{id_category}', 'API\TaskController@category');
Route::get('task/{id}', 'API\TaskController@show');
Route::put('task/{id}', 'API\TaskController@update');
Route::delete('task/{id}', 'API\TaskController@destroy');

Route::get('categories', 'API\CategoryController@index');